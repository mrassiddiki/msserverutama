# github.com/andybalholm/brotli v1.0.0
github.com/andybalholm/brotli
# github.com/dgrijalva/jwt-go v3.2.0+incompatible
## explicit
github.com/dgrijalva/jwt-go
github.com/dgrijalva/jwt-go/request
# github.com/go-sql-driver/mysql v1.5.0
## explicit
github.com/go-sql-driver/mysql
# github.com/gofiber/fiber/v2 v2.0.6
## explicit
github.com/gofiber/fiber/v2
github.com/gofiber/fiber/v2/internal/bytebufferpool
github.com/gofiber/fiber/v2/internal/colorable
github.com/gofiber/fiber/v2/internal/encoding/ascii
github.com/gofiber/fiber/v2/internal/encoding/json
github.com/gofiber/fiber/v2/internal/isatty
github.com/gofiber/fiber/v2/internal/schema
github.com/gofiber/fiber/v2/utils
# github.com/gorilla/mux v1.8.0
## explicit
github.com/gorilla/mux
# github.com/jmoiron/sqlx v1.2.0
## explicit
github.com/jmoiron/sqlx
github.com/jmoiron/sqlx/reflectx
# github.com/klauspost/compress v1.10.7
github.com/klauspost/compress/flate
github.com/klauspost/compress/gzip
github.com/klauspost/compress/zlib
# github.com/urfave/negroni v1.0.0
## explicit
github.com/urfave/negroni
# github.com/valyala/bytebufferpool v1.0.0
github.com/valyala/bytebufferpool
# github.com/valyala/fasthttp v1.16.0
github.com/valyala/fasthttp
github.com/valyala/fasthttp/fasthttputil
github.com/valyala/fasthttp/reuseport
github.com/valyala/fasthttp/stackless
# github.com/valyala/tcplisten v0.0.0-20161114210144-ceec8f93295a
github.com/valyala/tcplisten
# golang.org/x/sys v0.0.0-20200929083018-4d22bbb62b3c
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
